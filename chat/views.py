from django.shortcuts import render


def index(request):
    return render(request, 'chat/index.html')


def room(request, name):
    return render(request, 'chat/chatroom.html', {'room_name': name})
