from django.urls import path

from chat import views

urlpatterns = [
    path('', views.index, name="index"),
    path('<str:name>/', views.room, name="room")
]
